package pl.spring_learning.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.core.dao_abstraction.IProductDaoWriter;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.validator.DataValidator;
import pl.spring_learning.validator.ValidationException;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * @Author Lukasz Wojtas
 */
@Service
@Slf4j
public class KafkaProductService {

    @Autowired
    private IProductDaoWriter writer;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DataValidator dataValidator;

    private static final String PRODUCT_GROUP_ID = "PRODUCT";
    private static final String KAFKA_ERROR = "Failed to publish transaction result to kafka for message: ";

    @KafkaListener(topics = KafkaTopics.CREATE_PRODUCT, groupId = PRODUCT_GROUP_ID)
    public void createProducts(@Payload String message) {
        this.processMessage(message, writer::addProduct);
    }

    @KafkaListener(topics = KafkaTopics.DELETE_PRODUCT, groupId = PRODUCT_GROUP_ID)
    public void deleteProducts(@Payload String message) {
        this.processMessage(message, writer::removeProduct);
    }

    private void processMessage(String message, Consumer<ProductEntity> function) {
        KafkaTransactionMessage msg = null;
        try {
            msg = this.objectMapper
                    .readerFor(KafkaTransactionMessage.class).readValue(message);
            this.dataValidator.validate(msg);
            ProductEntity productEntity = this.objectMapper
                    .readerFor(ProductEntity.class).readValue(msg.getPayload());
            this.dataValidator.validate(productEntity);
            function.accept(productEntity);
            msg.setResult(KafkaTransactionResult.SUCCESS);
        } catch (JsonProcessingException | ValidationException e) {
            log.error("Error while deleting product: " + e.getMessage());
            Optional.ofNullable(msg).ifPresent(m -> m.setResult(KafkaTransactionResult.FAILED));
        }
        Optional.ofNullable(msg).ifPresent(m -> {
            String kafkaMessage = "";
            try {
                kafkaMessage = this.objectMapper.writerFor(KafkaTransactionMessage.class).writeValueAsString(m);
            } catch (JsonProcessingException ignored) {}
            String finalKafkaMessage = kafkaMessage;
            kafkaTemplate.send(KafkaTopics.PRODUCT_RESULT, kafkaMessage)
                    .completable()
                    .whenComplete((success, fail) -> {
                        if (fail != null) {
                            log.error(KAFKA_ERROR + finalKafkaMessage);
                        }
                    });
        });
    }
}
