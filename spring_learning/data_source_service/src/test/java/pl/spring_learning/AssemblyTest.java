package pl.spring_learning;

import org.junit.Ignore;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * @Author Lukasz Wojtas
 * */

@Ignore
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
public class AssemblyTest extends AbstractTestNGSpringContextTests {

    @Test(groups = {"AssemblyTests"})
    public void testDataSourceServiceAssembly() {}
}
