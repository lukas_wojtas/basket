package pl.spring_learning;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author Lukasz Wojtas
 * */

@Configuration
@ConfigurationProperties
public class ProductReaderServiceConfiguration {

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    @Primary
    public Map<String, ProductEntity> getProductList() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    @Primary
    public DataValidator getDataValidator() {
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(new Info().title("Product Reader Service API").description(
                        "This is description of REST interface which enable interaction with Product Reader Service"));
    }

}
