package pl.spring_learning.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.entity.AllProducts;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.services.ICacheService;

/**
 * @Author Lukasz Wojtas
 */

@Slf4j
@RestController
public class ProductRestController {

    @Autowired
    private ICacheService cacheService;

    @Operation(description = "Get all products from stock", tags = {"get products"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = AllProducts.class)))
    @RequestMapping(value = "/supermarket/allProducts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public AllProducts products() {
        return new AllProducts(this.cacheService.getProducts());
    }

    @Operation(description = "get product from stock by name", tags = {"get products"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = ProductEntity.class)))
    @ApiResponse(responseCode = "404", description = "Product not found in stock",
            content = @Content(schema = @Schema(implementation = RestControllerMessage.class)))
    @RequestMapping(value = "/supermarket/allProducts/product", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ProductEntity getSingleProduct(
            @Parameter(description = "Name of the product", required = true)
            @RequestParam(value = "name") String name) throws RestControllerException {
        return this.cacheService.getProductByName(name);
    }

    @ExceptionHandler({RestControllerException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public RestControllerMessage handleError(RestControllerException e) {
        return e.getRestControllerMessage();
    }
}
