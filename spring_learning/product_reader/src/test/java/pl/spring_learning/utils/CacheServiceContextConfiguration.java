package pl.spring_learning.utils;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import pl.spring_learning.core.dao_abstraction.IProductDaoReader;
import pl.spring_learning.definitions.KafkaTopics;

import java.util.Map;

/**
 * @Author Lukasz Wojtas
 * */
@Configuration
@EnableKafka
@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class})
public class CacheServiceContextConfiguration {

    private static final String TEST_GROUP_ID = "test_group_id";

    @Bean
    public ProducerFactory<String, String> producerFactory(EmbeddedKafkaBroker kafkaEmbedded) {
        return new DefaultKafkaProducerFactory<>(KafkaTestUtils.producerProps(kafkaEmbedded));
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate(EmbeddedKafkaBroker kafkaBroker) {
        KafkaTemplate<String, String> kafkaTemplate = new KafkaTemplate<>(producerFactory(kafkaBroker));
        kafkaTemplate.setDefaultTopic(KafkaTopics.PRODUCT_RESULT);
        return kafkaTemplate;
    }

    @Bean
    public ConsumerFactory<String, String> consumerFactory(EmbeddedKafkaBroker kafkaEmbedded) {
        Map<String, Object> props = KafkaTestUtils.consumerProps(TEST_GROUP_ID, "false", kafkaEmbedded);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory(EmbeddedKafkaBroker kafkaEmbedded) {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory(kafkaEmbedded));
        return factory;
    }

    @Bean
    IProductDaoReader getProductDaoReaderMock() {
        return Mockito.mock(IProductDaoReader.class);
    }

    @Bean
    TestDataLoader getTestDataLoader() {
        return new TestDataLoader();
    }
}
