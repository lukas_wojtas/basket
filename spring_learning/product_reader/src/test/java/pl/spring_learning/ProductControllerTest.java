package pl.spring_learning;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.controller.ProductRestController;
import pl.spring_learning.services.ICacheService;
import pl.spring_learning.utils.CommonContext;
import pl.spring_learning.utils.ProductsPojoFactory;
import pl.spring_learning.utils.ProductControllerContextBeanConfiguration;
import pl.spring_learning.utils.TestDataLoader;
import pl.spring_learning.entity.AllProducts;
import pl.spring_learning.model.RestControllerMessage;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @Author Lukasz Wojtas
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        ProductControllerContextBeanConfiguration.class,
        ProductRestController.class,
        CommonContext.class,
        ServiceSecurityConfiguration.class
})
@AutoConfigureMockMvc
public class ProductControllerTest {

    private static final String NO_PRODUCT_IN_STOCK = "No Product in stock";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductRestController restController;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ICacheService cacheService;

    @Before
    public void setUp() {
        reset(this.cacheService);
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testGetAllProducts() throws Exception {
        when(this.cacheService.getProducts()).thenReturn(new TestDataLoader().getProducts());
        mockMvc.perform(MockMvcRequestBuilders.get("/supermarket/allProducts")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(new AllProducts(new TestDataLoader().getProducts()))));
    }

    @Test
    @WithMockUser(value = "USER")
    public void testGetCola() throws Exception {
        when(this.cacheService.getProductByName(ProductsPojoFactory.COLA.getName()))
                .thenReturn(ProductsPojoFactory.COLA.getProduct());
        mockMvc.perform(MockMvcRequestBuilders.get("/supermarket/allProducts/product?name=cola")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(ProductsPojoFactory.COLA.getProduct())));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testProductNotInStock() throws Exception {
        when(this.cacheService.getProductByName(any()))
                .thenThrow(new RestControllerException(NO_PRODUCT_IN_STOCK));
        mockMvc.perform(MockMvcRequestBuilders.get("/supermarket/allProducts/product?name=aaaa")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content()
                        .json(mapper.writeValueAsString(new RestControllerMessage(NO_PRODUCT_IN_STOCK))));
    }
}
