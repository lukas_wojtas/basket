package pl.spring_learning;

import org.junit.Ignore;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * @Author Lukasz Wojtas
 * */

@Ignore
@SpringBootTest(properties = "spring.main.allow-bean-definition-overriding=true")
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {
                "listeners=PLAINTEXT://localhost:3333",
                "port=3333"
        })
public class AssemblyTest extends AbstractTestNGSpringContextTests {

    @Test(groups = {"AssemblyTests"})
    public void testProductReaderAssembly() {}
}