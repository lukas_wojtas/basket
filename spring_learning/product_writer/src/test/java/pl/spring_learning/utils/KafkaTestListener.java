package pl.spring_learning.utils;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import pl.spring_learning.definitions.KafkaTopics;

import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 * */
@Component
public class KafkaTestListener {

    public static final String TEST_GROUP_ID = "testGroupId";

    private String message;

    @KafkaListener(topics = KafkaTopics.PRODUCT_RESULT, groupId = TEST_GROUP_ID)
    public void getTransactionResult(@Payload String message) {
        this.message = message;
    }

    public boolean isMessageReceived() {
        return Optional.ofNullable(message).isPresent();
    }

    public String getMessage() {
        return this.message;
    }

    public void resetListener() {
        this.message = null;
    }
}
