package pl.spring_learning;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.services.TransactionService;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.utils.TransactionServiceContextConfiguration;

import java.util.Map;

/**
 * @Author Ewa SKiba
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {
        TransactionServiceContextConfiguration.class,
        TransactionService.class})
@EnableAutoConfiguration
@EnableScheduling
@ActiveProfiles("test")
public class TransactionServiceSetUpCacheTest {

    @Autowired
    private Map<String, TransactionDescription> transactionCache;

    @Before
    public void setUp() {
        transactionCache.clear();
    }

    @Test
    public void testSetUpCacheUpdateTransaction() throws InterruptedException {
        long now = System.currentTimeMillis();
        TransactionDescription transaction2 = new TransactionDescription(now, KafkaOperations.CREATE_PRODUCT, KafkaTransactionResult.IN_PROGRESS, "transaction6");
        this.transactionCache.put("transaction6", transaction2);
        Thread.sleep(2000);
        Assert.assertEquals(KafkaTransactionResult.FAILED, transaction2.getResult());
        Assert.assertTrue(transaction2.getTimestamp() > now);
        Assert.assertEquals(1, transactionCache.size());
        Thread.sleep(2000);
        Assert.assertFalse(transactionCache.containsKey("transaction6"));
        Assert.assertEquals(0, transactionCache.size());
    }

    @Test
    public void testSetUpCacheDeleteTransaction() throws InterruptedException {
        long now = System.currentTimeMillis();
        TransactionDescription transaction1 = new TransactionDescription(now, KafkaOperations.CREATE_PRODUCT, KafkaTransactionResult.SUCCESS, "transaction5");
        this.transactionCache.put("transaction5", transaction1);
        Thread.sleep(2000);
        Assert.assertFalse(transactionCache.containsKey("transaction5"));
        Assert.assertEquals(0, transactionCache.size());
    }
}
