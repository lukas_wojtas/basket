package pl.spring_learning;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.concurrent.SettableListenableFuture;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTopics;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.services.KafkaService;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.utils.KafkaServiceContextConfiguration;
import pl.spring_learning.utils.KafkaTestListener;
import pl.spring_learning.validator.DataValidator;
import pl.spring_learning.validator.ValidationException;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

import static org.awaitility.Awaitility.await;

/**
 * @Author Lukasz Wojtas
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {
        KafkaServiceContextConfiguration.class,
        KafkaTestListener.class,
        KafkaService.class
})
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {
                "listeners=PLAINTEXT://localhost:3333",
                "port=3333"
        })
@EnableAutoConfiguration
public class KafkaServiceTest {

    @Mock
    private Appender<ILoggingEvent> mockAppender;

    @Captor
    private ArgumentCaptor<ILoggingEvent> eventArgumentCaptor;

    @Captor
    private ArgumentCaptor<KafkaTransactionMessage> transactionMessageArgumentCaptor;

    @Autowired
    @SpyBean
    private KafkaTemplate<String, String> kafkaProducer;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private KafkaTestListener testListener;

    @Autowired
    private KafkaService kafkaService;

    @Autowired
    @SpyBean
    private ObjectMapper mapper;

    @SpyBean
    private DataValidator dataValidator;

    @Before
    public void setUp(){
        Mockito.reset(transactionService);
        Logger logger = (Logger) LoggerFactory.getLogger(KafkaService.class.getName());
        logger.addAppender(mockAppender);
    }

    /**
     * TODO: this send message to kafka and check if message was received.
     * Use it to test service.
     */
    @Test
    public void testSendAndReceiveMessage() {
        this.kafkaProducer.send(KafkaTopics.PRODUCT_RESULT, "message")
                .completable()
                .whenComplete((success, fail) -> Assert.assertNull(fail));
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> this.testListener.isMessageReceived());
    }

    @Test
    public void testSendingMessageSuccessful() {
        ArgumentCaptor<KafkaTransactionMessage> arg = ArgumentCaptor.forClass(KafkaTransactionMessage.class);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setPayload("payload");
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setTransactionId("transactionId");
        kafkaService.sendMessage(message, KafkaTopics.CREATE_PRODUCT);
        await()
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(this.transactionService, atMostOnce()).addTransaction(arg.capture());
                        if (Optional.ofNullable(arg.getValue()).isEmpty()) return false;
                        else return ((arg.getValue()).equals(message));
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testSendingMessageToKafkaFailed() throws JsonProcessingException {
        KafkaTransactionMessage transactionMessage = new KafkaTransactionMessage();
        transactionMessage.setPayload("payload");
        transactionMessage.setOperation(KafkaOperations.CREATE_PRODUCT);
        transactionMessage.setResult(KafkaTransactionResult.SUCCESS);
        transactionMessage.setTransactionId("transactionId");
        String message = this.mapper.writeValueAsString(transactionMessage);
        SettableListenableFuture<SendResult<String, String>> future = new SettableListenableFuture<>();
        future.setException(new Exception("TEST EXCEPTION"));
        Mockito.when(kafkaProducer.send(KafkaTopics.CREATE_PRODUCT, message)).thenReturn(future);
        kafkaService.sendMessage(transactionMessage, KafkaTopics.CREATE_PRODUCT);
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        verify(this.transactionService, atMostOnce()).addTransaction(transactionMessageArgumentCaptor.capture());
                        verify(mockAppender, atMostOnce()).doAppend(eventArgumentCaptor.capture());
                        if (Optional.ofNullable(transactionMessageArgumentCaptor.getValue()).isEmpty()) return false;
                        if (Optional.ofNullable(eventArgumentCaptor.getValue()).isEmpty()) return false;
                        else return ((eventArgumentCaptor.getValue().getMessage()).equals("Sending to kafka failed with : TEST EXCEPTION")) && ((transactionMessageArgumentCaptor.getValue().getResult()).equals(KafkaTransactionResult.FAILED));
                    } catch (MockitoException e) {
                        return false;
                    }
                });
    }

    @Test
    public void testSendingMessageWithMappingException() throws JsonProcessingException {
        KafkaTransactionMessage transactionMessage = new KafkaTransactionMessage();
        Mockito.when(mapper.writeValueAsString(any())).thenThrow(new JsonProcessingException("Test"){});
        kafkaService.sendMessage(transactionMessage, KafkaTopics.CREATE_PRODUCT);
        verify(mockAppender).doAppend(eventArgumentCaptor.capture());
        Assert.assertEquals("Mapping Kafka Transaction message failed with: Test", eventArgumentCaptor.getValue().getMessage());
    }

    @Test
    public void testGetTransactionResultSuccessful() throws JsonProcessingException {
        ProductEntity entity = new ProductEntity();
        entity.setName("cola");
        entity.setPrice(100);
        String productJson = this.mapper.writer().writeValueAsString(entity);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setTransactionId("transactionId");
        message.setResult(KafkaTransactionResult.IN_PROGRESS);
        message.setPayload(productJson);
        TransactionDescription transactionDescription = new TransactionDescription(System.currentTimeMillis(), KafkaOperations.DELETE_PRODUCT, KafkaTransactionResult.SUCCESS, message.getTransactionId());
        Mockito.when(this.transactionService.getTransaction(message.getTransactionId())).thenReturn(Optional.of(transactionDescription));
        kafkaService.getTransactionResult(this.mapper.writeValueAsString(message));
        verify(this.transactionService, atMostOnce()).getTransaction(message.getTransactionId());
        Assert.assertEquals(transactionDescription.getResult(), message.getResult());
    }

    @Test
    public void testGetTransactionResultWithValidationException() throws ValidationException, JsonProcessingException {
        ProductEntity entity = new ProductEntity();
        entity.setName("cola");
        entity.setPrice(100);
        String productJson = this.mapper.writer().writeValueAsString(entity);
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setTransactionId("transactionId");
        message.setResult(KafkaTransactionResult.IN_PROGRESS);
        message.setPayload(productJson);
        Mockito.when(dataValidator.validate(this.mapper.writeValueAsString(message))).thenThrow(new ValidationException("Test"));
        kafkaService.getTransactionResult(this.mapper.writeValueAsString(message));
        verify(mockAppender).doAppend(eventArgumentCaptor.capture());
        Assert.assertEquals("Kafka message unknown Test", eventArgumentCaptor.getValue().getMessage());
    }

}
