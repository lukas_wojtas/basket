package pl.spring_learning;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.controllers.ProductWriterRestController;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.properties.ProductWriterProperties;
import pl.spring_learning.services.IKafkaService;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.utils.ProductWriterControllersBeanConfiguration;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @Author Ewa Skiba
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        ProductWriterControllersBeanConfiguration.class,
        ProductWriterRestController.class,
        ProductWriterProperties.class,
        ServiceSecurityConfiguration.class
})
@AutoConfigureMockMvc
@TestPropertySource(properties = {
        "writer.port=8014",
        "writer.host=localhost"
})
@EnableWebMvc
public class ProductWriterRestControllerTest {

    private static final String REQUEST_FAILED = "Processing request failed";

    private static final ProductEntity PRODUCT = new ProductEntity();

    @Autowired
    private MockMvc mockMvc;

    @SpyBean
    private ObjectMapper spyMapper;

    @Autowired
    private IKafkaService kafkaService;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private ProductWriterRestController productWriterRestController;

    @Before
    public void setUp() {
        PRODUCT.setName("bike");
        PRODUCT.setPrice(300);
    }


    @Test
    public void testCreateProduct() {
        ResponseEntity<Void> responseEntity = productWriterRestController.createProduct(PRODUCT);
        Assert.assertTrue(responseEntity.getHeaders().toString().contains("transactionstatus?id"));
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.SEE_OTHER);
    }

    @Test
    public void testDeleteProduct() {
        ResponseEntity<Void> responseEntity = productWriterRestController.deleteProduct("cola");
        Assert.assertTrue(responseEntity.getHeaders().toString().contains("transactionstatus?id"));
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.SEE_OTHER);
    }

    @Test
    public void testDeleteProductWithException() throws IOException {
        final ObjectWriter writer = mock(ObjectWriter.class);
        Mockito.when(spyMapper.writerFor(ProductEntity.class)).thenReturn(writer);
        Mockito.when(writer.writeValueAsString(any())).thenThrow(new JsonProcessingException("") {
        });
        RestControllerException exception = assertThrows(RestControllerException.class,
                () -> productWriterRestController.deleteProduct("cola"));
        Assert.assertEquals(REQUEST_FAILED, exception.getRestControllerMessage().getMessage());
    }

    @Test
    public void testCreateProductWithException() throws IOException {
        final ObjectWriter writer = mock(ObjectWriter.class);
        Mockito.when(spyMapper.writerFor(ProductEntity.class)).thenReturn(writer);
        Mockito.when(writer.writeValueAsString(any())).thenThrow(new JsonProcessingException("") {
        });
        RestControllerException exception = assertThrows(RestControllerException.class,
                () -> productWriterRestController.createProduct(PRODUCT));
        Assert.assertEquals(REQUEST_FAILED, exception.getRestControllerMessage().getMessage());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testRequestError() throws Exception {
        String json = this.spyMapper.writerFor(ProductEntity.class).writeValueAsString(PRODUCT);
        final ObjectWriter writer = mock(ObjectWriter.class);
        Mockito.when(spyMapper.writerFor(ProductEntity.class)).thenReturn(writer);
        Mockito.when(writer.writeValueAsString(any())).thenThrow(new JsonProcessingException("") {});
        mockMvc.perform(MockMvcRequestBuilders.put("/admin/createProduct")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .characterEncoding("utf-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(spyMapper.writeValueAsString(new RestControllerMessage(REQUEST_FAILED))));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testDelete() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/admin/deleteProduct?name=name")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testPut() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/admin/deleteProduct?name=name")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void testUserForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/admin/deleteProduct?name=name")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
