package pl.spring_learning.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Lukasz Wojtas
 * */
@ConfigurationProperties(prefix = "cache")
@Data
@Configuration
public class TransactionCacheProperties {
    private String refresh;
    private Integer ttl;
}
