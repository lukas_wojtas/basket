package pl.spring_learning.transaction;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTransactionResult;

/**
 * @Author Lukasz Wojtas
 * */
@AllArgsConstructor
@Getter
public class TransactionDescription {
    @Schema(description = "time of operation")
    @Setter
    private long timestamp;
    @Schema(description = "performed operation")
    private final KafkaOperations operation;
    @Setter
    @Schema(description = "transaction result")
    private KafkaTransactionResult result;
    @Schema(description = "transaction identity")
    private final String transactionId;
}
