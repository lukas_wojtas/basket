package pl.spring_learning.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.*;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.model.RestControllerMessage;
import pl.spring_learning.services.ITransactionService;
import pl.spring_learning.transaction.TransactionDescription;

/**
 * @Author Lukasz Wojtas
 */

@RestController
@EnableScheduling
@Slf4j
public class OperationResultRestController {

    public static final String PATH = "/transactionstatus";

    @Autowired
    private ITransactionService transactionService;

    @Operation(description = "get transaction status", tags = {"transaction"})
    @ApiResponse(responseCode = "200", description = "successful operation",
            content = @Content(schema = @Schema(implementation = TransactionDescription.class)))
    @ApiResponse(responseCode = "404", description = "Transaction not found",
            content = @Content(schema = @Schema(implementation = RestControllerMessage.class)))
    @RequestMapping(path=PATH, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public TransactionDescription getTransactionStatus(
            @Parameter(description = "transaction identity", required = true)
            @RequestParam(name="id") String id) {
        return this.transactionService.getTransaction(id)
                .orElseThrow(() -> new RestControllerException("Transaction not found"));
    }

    @ExceptionHandler({RestControllerException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestControllerMessage handleError(RestControllerException e) {
        return e.getRestControllerMessage();
    }

}
