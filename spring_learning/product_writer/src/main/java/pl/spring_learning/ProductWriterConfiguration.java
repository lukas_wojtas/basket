package pl.spring_learning;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.jetty.JettyServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import pl.spring_learning.properties.ProductWriterProperties;
import pl.spring_learning.transaction.TransactionDescription;
import pl.spring_learning.validator.DataValidator;

import javax.validation.Validation;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author Lukasz Wojtas
 * */

@Configuration
public class ProductWriterConfiguration {

    @Bean
    public ConfigurableServletWebServerFactory containerCustomizer(ProductWriterProperties props) {
        return new JettyServletWebServerFactory(props.getPort());
    }

    @Bean
    @Primary
    ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    @Primary
    DataValidator getValidator() {
        return new DataValidator(Validation.buildDefaultValidatorFactory());
    }

    @Bean
    Map<String, TransactionDescription> getTransactionResultCache() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(new Info().title("Product Writer Service API").description(
                        "This is description of REST interface which enable interaction with Product Writer Service"));
    }
}
