package pl.spring_learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author Lukasz Wojtas
 * */
@SpringBootApplication
@EnableAutoConfiguration
public class ProductWriterService {

    public static void main(String[] args) {
        SpringApplication.run(ProductWriterService.class, args);
    }
}
