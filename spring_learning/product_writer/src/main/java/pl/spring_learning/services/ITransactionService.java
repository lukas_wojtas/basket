package pl.spring_learning.services;

import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.transaction.TransactionDescription;

import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 * */
public interface ITransactionService {

    void addTransaction(KafkaTransactionMessage message);

    Optional<TransactionDescription> getTransaction(String id);
}
