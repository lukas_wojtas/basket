package pl.spring_learning.services;

import org.springframework.stereotype.Service;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;

/**
 * @Author Lukasz Wojtas
 * */
public interface IKafkaService {
    void sendMessage(KafkaTransactionMessage message, String topic);
}
