#set up variables
PRODUCT_READER_DIR=../../../product_reader/src/main/resources
PRODUCT_WRITER_DIR=../../../product_writer/src/main/resources
BASKET_SERVICE_DIR=../../../basket_service/src/main/resources
PRODUCT_READER=$PRODUCT_READER_DIR/product-reader.p12
PRODUCT_WRITER=$PRODUCT_WRITER_DIR/product-writer.p12
BASKET_SERVICE=$BASKET_SERVICE_DIR/basket-service.p12

#clear up possible previous attempts
rm $PRODUCT_READER
rm $PRODUCT_WRITER
rm $BASKET_SERVICE

#Generate my own cert
keytool -genkeypair -alias product-reader -keyalg RSA -keysize 2048 -dname "CN=reader,OU=reader,O=reader,L=cracow,S=lesser,C=pl" -storetype PKCS12 -keypass password -keystore $PRODUCT_READER -storepass password -validity 3650
keytool -genkeypair -alias product-writer -keyalg RSA -keysize 2048 -dname "CN=reader,OU=reader,O=writer,L=cracow,S=lesser,C=pl" -storetype PKCS12 -keypass password -keystore $PRODUCT_WRITER -storepass password -validity 3650
keytool -genkeypair -alias basket-service -keyalg RSA -keysize 2048 -dname "CN=reader,OU=reader,O=writer,L=cracow,S=lesser,C=pl" -storetype PKCS12 -keypass password -keystore $BASKET_SERVICE -storepass password -validity 3650

# Export public certificates for both the client and server
keytool -exportcert -alias product-reader -file product-reader.cer -keystore $PRODUCT_READER -storepass password
#keytool -exportcert -alias product-writer -file product-writer.cer -keystore $PRODUCT_WRITER -storepass password
keytool -exportcert -alias basket-service -file basket-service.cer -keystore $BASKET_SERVICE -storepass password

# Import the basket-service public certificate into product reader trust store
keytool -importcert -keystore $PRODUCT_READER -alias basket-service -file basket-service.cer -storepass password -noprompt

#Import the product-reader public certificate into basket service trust store
keytool -importcert -keystore $BASKET_SERVICE -alias product-reader -file product-reader.cer -storepass password -noprompt

#clean up public keys
rm product-reader.cer
rm basket-service.cer