package pl.spring_learning.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Lukasz Wojtas
 */
public class ProductEntityTest {

    private static Map<String, ProductEntity> cache;

    @Before
    public void setUp() {
        cache = new HashMap<>();
        ProductEntity p = new ProductEntity();
        p.setName("name");
        p.setSpecialPrice(33);
        p.setPrice(33);
        p.setAmount(33);
        cache.put(p.getName(), p);
    }

    @Test
    public void testRemovingProductInCache() {
        cache.remove("name");
        Assert.assertEquals(0, cache.size());
    }

    @Test
    public void testReplaceProductInCache() {
        ProductEntity p = new ProductEntity();
        p.setName("name");
        p.setSpecialPrice(11);
        p.setPrice(1);
        p.setAmount(1);
        cache.replace(p.getName(), p);
        Assert.assertEquals(1, cache.size());
        Assert.assertEquals(11, cache.get(p.getName()).getSpecialPrice().intValue());
        Assert.assertEquals(1, cache.get(p.getName()).getPrice().intValue());
        Assert.assertEquals(1, cache.get(p.getName()).getAmount().intValue());
    }
}
