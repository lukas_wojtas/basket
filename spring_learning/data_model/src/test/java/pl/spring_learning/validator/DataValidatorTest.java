package pl.spring_learning.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.spring_learning.entity.ProductEntity;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;

/**
 * @Author Lukasz Wojtas
 * */

public class DataValidatorTest {

    private ValidatorFactory validatorFactory;

    @Before
    public void setUp() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
    }

    @Test
    public void testValidationOfFullCorrectObject() throws ValidationException {
        ProductEntity product = new ProductEntity();
        product.setAmount(10);
        product.setName("name");
        product.setSpecialPrice(10);
        product.setPrice(15);
        DataValidator validator = new DataValidator(this.validatorFactory);
        Assert.assertTrue(validator.validate(product));
    }

    @Test
    public void testValidationOfProductWithoutSpecialPrize() throws ValidationException {
        ProductEntity product = new ProductEntity();
        product.setName("name");
        product.setPrice(15);
        DataValidator validator = new DataValidator(this.validatorFactory);
        Assert.assertTrue(validator.validate(product));
    }

    @Test(expected = ValidationException.class)
    public void testValidationProductWithoutName() throws ValidationException {
        ProductEntity product = new ProductEntity();
        product.setAmount(10);
        product.setSpecialPrice(10);
        product.setPrice(15);
        new DataValidator(this.validatorFactory).validate(product);
    }

    @Test(expected = ValidationException.class)
    public void testValidationProductWithEmptyName() throws ValidationException {
        ProductEntity product = new ProductEntity();
        product.setName("");
        product.setAmount(10);
        product.setSpecialPrice(10);
        product.setPrice(15);
        new DataValidator(this.validatorFactory).validate(product);
    }
}
