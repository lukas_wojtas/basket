package pl.spring_learning.utils;

import pl.spring_learning.entity.ProductEntity;

/**
 * @Author Lukasz Wojtas
 * */
public enum ProductsPojoFactory {

    BREAD("bread") {
        @Override
        public ProductEntity getProduct() {
            ProductEntity product = new ProductEntity();
            product.setName(BREAD.name);
            product.setPrice(10);
            product.setSpecialPrice(15);
            product.setAmount(3);
            return product;
        }

        @Override
        public String getName() {
            return BREAD.name;
        }
    },
    COLA("cola") {
        @Override
        public ProductEntity getProduct() {
            ProductEntity product = new ProductEntity();
            product.setName(COLA.name);
            product.setPrice(5);
            product.setSpecialPrice(20);
            product.setAmount(5);
            return product;
        }

        @Override
        public String getName() {
            return COLA.name;
        }
    },
    SKI("ski") {
        @Override
        public ProductEntity getProduct() {
            ProductEntity product = new ProductEntity();
            product.setAmount(0);
            product.setSpecialPrice(0);
            product.setName(SKI.name);
            product.setPrice(40);
            return product;
        }

        @Override
        public String getName() {
            return SKI.name;
        }
    },
    BUTTER("butter") {
        @Override
        public ProductEntity getProduct() {
            ProductEntity product = new ProductEntity();
            product.setAmount(4);
            product.setSpecialPrice(50);
            product.setName(BUTTER.name);
            product.setPrice(15);
            return product;
        }

        @Override
        public String getName() {
            return BUTTER.name;
        }
    },
    SNOWBOARD("snowboard") {
        @Override
        public ProductEntity getProduct() {
            ProductEntity product = new ProductEntity();
            product.setAmount(0);
            product.setName(SNOWBOARD.name);
            product.setSpecialPrice(0);
            product.setPrice(30);
            return product;
        }

        @Override
        public String getName() {
            return SNOWBOARD.name;
        }
    },
    MOBILE("mobile") {
        @Override
        public ProductEntity getProduct() {
            ProductEntity product = new ProductEntity();
            product.setAmount(0);
            product.setPrice(5);
            product.setSpecialPrice(0);
            product.setName(MOBILE.name);
            return product;
        }

        @Override
        public String getName() {
            return MOBILE.name;
        }
    };

    private final String name;

    ProductsPojoFactory(String name) {
        this.name = name;
    }

    public abstract ProductEntity getProduct();

    public abstract String getName();
}
