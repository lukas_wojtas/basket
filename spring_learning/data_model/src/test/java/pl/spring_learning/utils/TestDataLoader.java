package pl.spring_learning.utils;

import pl.spring_learning.entity.ProductEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author Lukasz Wojtas
 * */
public class TestDataLoader {

    private final Map<String, ProductEntity> cache = new HashMap<>();

    public TestDataLoader() {
        loadProducts();
    }

    private ProductEntity getProduct(String name) {
        return cache.get(name.toLowerCase());
    }

    public List<ProductEntity> getProducts() {
        return cache.keySet().stream().map(this::getProduct).collect(Collectors.toList());
    }

    private void loadProducts() {
        for (ProductsPojoFactory product : ProductsPojoFactory.values()) {
            cache.put(product.getName(), product.getProduct());
        }
    }
}
