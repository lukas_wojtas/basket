package pl.spring_learning;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.context.junit4.SpringRunner;
/**
 * @Author Lukasz Wojtas
 * */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceSecurityConfiguration.class)
@EnableConfigurationProperties
@EnableWebSecurity
public class TestSecurityConfigurationLoadingProps {

    @Autowired
    private ServiceSecurityConfiguration config;

    @Test
    public void testLoadProperties() {
        Assert.assertEquals(2, config.getUsers().size());
        Assert.assertEquals("user1", config.getUsers().get(0).getUser());
        Assert.assertEquals("pass1", config.getUsers().get(0).getPassword());
        Assert.assertEquals(1, config.getUsers().get(0).getRoles().size());
        Assert.assertEquals("user2", config.getUsers().get(1).getUser());
        Assert.assertEquals("pass2", config.getUsers().get(1).getPassword());
        Assert.assertEquals(2, config.getUsers().get(1).getRoles().size());
        Assert.assertEquals(2, config.getUrls().size());
        Assert.assertEquals("role1", config.getUrls().get(0).getRole());
        Assert.assertEquals(2, config.getUrls().get(0).getUrls().size());
        Assert.assertEquals("get", config.getUrls().get(0).getHttpMethod());
        Assert.assertEquals("role2", config.getUrls().get(1).getRole());
        Assert.assertEquals(1, config.getUrls().get(1).getUrls().size());
        Assert.assertEquals("post", config.getUrls().get(1).getHttpMethod());
    }
}
