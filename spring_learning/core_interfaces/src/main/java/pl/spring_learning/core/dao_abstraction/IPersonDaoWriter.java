package pl.spring_learning.core.dao_abstraction;

import pl.spring_learning.entity.PersonEntity;
import java.util.List;
import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 * */

public interface IPersonDaoWriter {

    void savePerson(PersonEntity person);

    void removePerson(PersonEntity person);

    Optional<PersonEntity> getPerson(Integer id);

    void removePeople(List<PersonEntity> people);
}
