package pl.spring_learning.core.dao_abstraction;

import pl.spring_learning.entity.ProductEntity;

import java.util.List;
import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 * */

public interface IProductDaoReader {

    Optional<ProductEntity> getProduct(String name);

    List<ProductEntity> getAllProducts();

    Boolean checkIfProductInStock(String name);

}
