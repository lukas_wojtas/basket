package pl.spring_learning.controler_exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.spring_learning.model.RestControllerMessage;

/**
 * @Author Lukasz Wojtas
 * */
public class RestControllerException extends RuntimeException {

    @Getter
    private final RestControllerMessage restControllerMessage;

    public RestControllerException(String message) {
        this.restControllerMessage = new RestControllerMessage(message);
    }
}
