package pl.spring_learning;

import org.junit.Assert;
import org.junit.Test;
import pl.spring_learning.controler_exceptions.RestControllerException;

/**
 * @Author Lukasz Wojtas
 * */
public class TestControllerException {

    @Test
    public void testExceptionConstructProperly() {
        try {
            throw new RestControllerException("msg");
        } catch (RestControllerException e) {
            Assert.assertNotNull(e.getRestControllerMessage());
            Assert.assertEquals("msg",
                    e.getRestControllerMessage().getMessage());
        }
    }
}
