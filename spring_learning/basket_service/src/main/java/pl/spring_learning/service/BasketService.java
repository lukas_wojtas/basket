package pl.spring_learning.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.model.Basket;
import pl.spring_learning.model.BasketGood;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author Lukasz Wojtas
 */

@Slf4j
@Service
@Primary
public class BasketService implements IBasketService {

    private final Map<String, Pair<ProductEntity, Integer>> basketCache = new HashMap<>();

    private final IProductServiceConnector productServiceConnector;

    @Autowired
    public BasketService(IProductServiceConnector productServiceConnector) {
        this.productServiceConnector = productServiceConnector;
    }

    @Override
    public void addProduct(String consumerGood, int amount) {
        if (amount <= 0)
            return;
        ProductEntity product = productServiceConnector.getProduct(consumerGood);
        if (basketCache.containsKey(product.getName())) {
            Pair<ProductEntity, Integer> cachedProduct = this.basketCache.get(product.getName());
            int totalAmount = cachedProduct.getRight() + amount;
            if (product.equals(cachedProduct.getKey())) {
                cachedProduct.setValue(totalAmount);
            } else {
                this.basketCache.replace(product.getName(),
                        basketCache.get(product.getName()), MutablePair.of(product, totalAmount));
            }
        } else {
            basketCache.put(product.getName(), MutablePair.of(product, amount));
        }
    }

    @Override
    public void removeProduct(String consumerGood, int amount) {
        if (amount <= 0)
            return;
        if (this.basketCache.containsKey(consumerGood)) {
            Pair<ProductEntity, Integer> cachedProducts = this.basketCache.get(consumerGood);
            int totalAmount = cachedProducts.getRight() - amount;
            if (totalAmount <= 0) {
                this.basketCache.remove(consumerGood);
            } else {
                cachedProducts.setValue(totalAmount);
            }
        }
    }

    private int calculatePriceForProduct(Pair<ProductEntity, Integer> cachedProduct) {
        if (isSpecialOfferAvailable(cachedProduct.getLeft())) {
            int numberOfPromoPriceProducts = cachedProduct.getRight() / cachedProduct.getLeft().getAmount();
            int numberOfNormalPriceProducts = cachedProduct.getRight() % cachedProduct.getLeft().getAmount();
            return (numberOfPromoPriceProducts * cachedProduct.getLeft().getSpecialPrice())
                    + numberOfNormalPriceProducts * cachedProduct.getLeft().getPrice();
        } else {
            return cachedProduct.getRight() * cachedProduct.getLeft().getPrice();
        }
    }

    @Override
    public void clearBasket() {
        basketCache.clear();
    }

    private boolean isSpecialOfferAvailable(ProductEntity product) {
        return product.getSpecialPrice() != 0 && product.getAmount() != 0;
    }

    @Override
    public Basket getBasket() {
        List<Pair<String, Integer>> productNamesWithPrices =
                this.basketCache
                        .keySet()
                        .stream()
                        .map(this.basketCache::get)
                        .map(cachedProduct ->
                                MutablePair.of(cachedProduct.getLeft().getName(), calculatePriceForProduct(cachedProduct)))
                        .collect(Collectors.toList());
        int totalPrice = productNamesWithPrices.stream().map(Pair::getRight).reduce(0, Integer::sum);
        List<BasketGood> basketGoodList = productNamesWithPrices.stream().map(productsWithNames -> new BasketGood(
                this.basketCache.get(productsWithNames.getLeft()).getRight(),
                productsWithNames.getRight(),
                productsWithNames.getLeft()
        )).collect(Collectors.toList());
        return new Basket(basketGoodList, totalPrice);
    }
}
