package pl.spring_learning.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.properties.ProductServiceProperties;

import java.io.IOException;
import java.util.Optional;

/**
 * @Author Lukasz Wojtas
 */

@Slf4j
@Service
@Primary
public class ProductServiceConnector implements IProductServiceConnector {

    public static final String PRODUCT_NOT_FOUND = "Product not found in stock.";
    public static final String NO_CONNECTION_TO_HOST = "No connection to store";

    private final String singleProductAddress;

    private final OkHttpClient httpClient;

    private final ObjectMapper mapper;

    @Autowired
    public ProductServiceConnector(ProductServiceProperties props, OkHttpClient httpClient,
                                   ObjectMapper mapper) {
        singleProductAddress = "https://" + props.getHost() + ":"
                + props.getPort() + "/supermarket/allProducts/product?name=";
        this.httpClient = httpClient;
        this.mapper = mapper;
    }

    @Override
    public ProductEntity getProduct(String name) throws RestControllerException {
        Request request = new Request.Builder().url(singleProductAddress + name).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.isSuccessful() && Optional.ofNullable(response.body()).isPresent()) {
                return mapper.readValue(response.body().string(), ProductEntity.class);
            } else {
                log.error("Product Request failed with : " + response.code());
                throw new RestControllerException(PRODUCT_NOT_FOUND);
            }
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
            throw new RestControllerException(NO_CONNECTION_TO_HOST);
        }
    }
}
