package pl.spring_learning.service;

import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.model.Basket;

/**
 * @Author Lukasz Wojtas
 */

public interface IBasketService {

    void addProduct(String consumerGood, int amount) throws RestControllerException;

    void removeProduct(String consumerGood, int amount);

    void clearBasket();

    Basket getBasket();
}
