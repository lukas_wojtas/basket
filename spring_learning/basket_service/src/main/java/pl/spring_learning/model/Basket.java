package pl.spring_learning.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import java.util.List;

/**
 * @Author Lukasz Wojtas
 */

@Getter
public class Basket {

    @Schema(description = "List of goods in Basket")
    private final List<BasketGood> basket;

    @Schema(description = "Total price of all goods in Basket")
    private final int totalPrice;

    public Basket(List<BasketGood> basket, int totalPrice) {
        this.totalPrice = totalPrice;
        this.basket = basket;
    }
}
