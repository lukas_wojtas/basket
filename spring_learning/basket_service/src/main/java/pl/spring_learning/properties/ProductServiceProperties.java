package pl.spring_learning.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * @Author Lukasz Wojtas
 * */
@ConfigurationProperties(prefix = "product")
@Data
@Configuration
public class ProductServiceProperties {
    private int port;
    private String host;
    private String user;
    private String password;
}
