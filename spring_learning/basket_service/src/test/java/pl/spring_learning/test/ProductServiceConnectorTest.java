package pl.spring_learning.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.service.ProductServiceConnector;
import pl.spring_learning.utils.OkHttpClientConfiguration;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @Author Ewa Skiba
 */

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        OkHttpClientConfiguration.class
})
public class ProductServiceConnectorTest {

    @Autowired
    private ProductServiceConnector productServiceConnector;

    @Autowired
    private OkHttpClient okHttpClient;

    private final Call call = mock(Call.class);

    private final ProductEntity cola = new ProductEntity();

    @Before
    public void setup() {
        reset(okHttpClient);
        reset(call);
        cola.setName("cola");
        cola.setPrice(15);
    }

    @Test
    public void testNoConnectionException() throws IOException {
        Response response = new Response.Builder()
                .code(200)
                .protocol(Protocol.HTTP_1_1)
                .message("Response.success()")
                .body(ResponseBody.create(MediaType.parse("application/json"), "requestedProduct"))
                .request(new Request.Builder().url("http:/supermarket/allProducts/product?name=cola").build())
                .build();
        when(call.execute()).thenReturn(response);
        when(okHttpClient.newCall(any())).thenReturn(call);

        RestControllerException exception = assertThrows(RestControllerException.class, () -> productServiceConnector.getProduct("cola"));
        Assert.assertEquals(ProductServiceConnector.NO_CONNECTION_TO_HOST, exception.getRestControllerMessage().getMessage());
    }

    @Test
    public void testProductSuccessfullyReturned() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String requestedProduct = mapper.writeValueAsString(cola);

        Response response = new Response.Builder()
                .code(200)
                .protocol(Protocol.HTTP_1_1)
                .message("Response.success()")
                .body(ResponseBody.create(MediaType.parse("application/json"), requestedProduct))
                .request(new Request.Builder().url("http:/supermarket/allProducts/product?name=cola").build())
                .build();
        when(call.execute()).thenReturn(response);
        when(okHttpClient.newCall(any())).thenReturn(call);
        ProductEntity returnedProduct = productServiceConnector.getProduct("cola");
        Assert.assertEquals(cola.getName(), returnedProduct.getName());
        Assert.assertEquals(cola.getPrice(), returnedProduct.getPrice());
        Assert.assertEquals(cola.getAmount(), returnedProduct.getAmount());
        Assert.assertEquals(cola.getSpecialPrice(), returnedProduct.getSpecialPrice());
    }

    @Test
    public void testProductNotFoundNoResponseBody() throws IOException {
        Response response = new Response.Builder()
                .code(200)
                .protocol(Protocol.HTTP_1_1)
                .message("Response.success()")
                .request(new Request.Builder().url("http:/supermarket/allProducts/product?name=cola").build())
                .build();
        when(call.execute()).thenReturn(response);
        when(okHttpClient.newCall(any())).thenReturn(call);
        RestControllerException exception = assertThrows(RestControllerException.class, () -> {
            productServiceConnector.getProduct("cola");
        });
        Assert.assertEquals(ProductServiceConnector.PRODUCT_NOT_FOUND, exception.getRestControllerMessage().getMessage());
    }

    @Test
    public void testProductNotFoundUnsuccessfulResponse() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String requestedProduct = mapper.writeValueAsString(cola);

        Response response = new Response.Builder()
                .code(404)
                .protocol(Protocol.HTTP_1_1)
                .message("Response.success()")
                .body(ResponseBody.create(MediaType.parse("application/json"), requestedProduct))
                .request(new Request.Builder().url("http:/supermarket/allProducts/product?name=cola").build())
                .build();
        when(call.execute()).thenReturn(response);
        when(okHttpClient.newCall(any())).thenReturn(call);
        RestControllerException exception = assertThrows(RestControllerException.class, () -> {
            productServiceConnector.getProduct("cola");
        });
        Assert.assertEquals(ProductServiceConnector.PRODUCT_NOT_FOUND, exception.getRestControllerMessage().getMessage());
    }
}
