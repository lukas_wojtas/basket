package pl.spring_learning.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.spring_learning.controler_exceptions.RestControllerException;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.model.BasketGood;
import pl.spring_learning.service.BasketService;
import pl.spring_learning.utils.TestContextBeanConfiguration;

import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.spring_learning.utils.ProductsPojoFactory.BREAD;
import static pl.spring_learning.utils.ProductsPojoFactory.BUTTER;
import static pl.spring_learning.utils.ProductsPojoFactory.COLA;
import static pl.spring_learning.utils.ProductsPojoFactory.MOBILE;

/**
 * @Author Lukasz Wojtas
 */

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={TestContextBeanConfiguration.class})
@SpringBootTest
public class BasketServiceTest {

    @Autowired
    private BasketService basket;

    @Before
    public void setUp() {
        basket.clearBasket();
        reset(TestContextBeanConfiguration.getProductServiceConnector());
    }

    @Test
    public void testAddingZeroProducts() throws RestControllerException{
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        basket.addProduct(BREAD.getProduct().getName(), 0);
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        Assert.assertEquals(0, basket.getBasket().getTotalPrice());
        verify(TestContextBeanConfiguration.getProductServiceConnector(),
                times(0)).getProduct(BREAD.getName());
    }

    @Test
    public void testAddingRemovingConsumerGoodFromBasket() throws RestControllerException {
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        basket.addProduct(BREAD.getProduct().getName(), 1);
        Assert.assertEquals(1, basket.getBasket().getBasket().size());
        Assert.assertEquals(1, basket.getBasket().getBasket().get(0).getQuantity());
        Assert.assertEquals(10, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(BREAD.getName(), basket.getBasket().getBasket().get(0).getName());
        Assert.assertEquals(10, basket.getBasket().getTotalPrice());
        basket.removeProduct(BREAD.getName(), 1);
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(1)).getProduct(BREAD.getName());
    }

    @Test
    public void testBasketUpdateAfterAddingModifiedProduct() throws RestControllerException {
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        basket.addProduct(BREAD.getProduct().getName(), 1);
        Assert.assertEquals(1, basket.getBasket().getBasket().size());
        Assert.assertEquals(1, basket.getBasket().getBasket().get(0).getQuantity());
        Assert.assertEquals(10, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(BREAD.getName(), basket.getBasket().getBasket().get(0).getName());
        Assert.assertEquals(10, basket.getBasket().getTotalPrice());
        ProductEntity newBread = new ProductEntity();
        newBread.setName("bread");
        newBread.setPrice(15);
        newBread.setAmount(2);
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BREAD.getName())).thenReturn(newBread);
        basket.addProduct(newBread.getName(), 1);
        Assert.assertEquals(1, basket.getBasket().getBasket().size());
        Assert.assertEquals(2, basket.getBasket().getBasket().get(0).getQuantity());
        Assert.assertEquals(30, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(BREAD.getName(), basket.getBasket().getBasket().get(0).getName());
        Assert.assertEquals(30, basket.getBasket().getTotalPrice());
        basket.removeProduct(BREAD.getName(), 2);
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(2)).getProduct(BREAD.getName());
    }

    @Test
    public void testAddingAndRemovingVariousGoodsFromBasket() throws RestControllerException {
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BUTTER.getName())).thenReturn(BUTTER.getProduct());
        basket.addProduct(COLA.getName(), 1);
        basket.addProduct(BREAD.getName(), 1);
        basket.addProduct(BUTTER.getName(), 1);
        Assert.assertEquals(3, basket.getBasket().getBasket().size());
        Assert.assertEquals(1, this.getFromBasket(COLA.getName()).getQuantity());
        Assert.assertEquals(5, this.getFromBasket(COLA.getName()).getPriceForProduct());
        Assert.assertEquals(COLA.getName(), this.getFromBasket(COLA.getName()).getName());
        Assert.assertEquals(1, this.getFromBasket(BREAD.getName()).getQuantity());
        Assert.assertEquals(10, this.getFromBasket(BREAD.getName()).getPriceForProduct());
        Assert.assertEquals(BREAD.getName(), this.getFromBasket(BREAD.getName()).getName());
        Assert.assertEquals(1, this.getFromBasket(BUTTER.getName()).getQuantity());
        Assert.assertEquals(15, this.getFromBasket(BUTTER.getName()).getPriceForProduct());
        Assert.assertEquals(BUTTER.getName(), this.getFromBasket(BUTTER.getName()).getName());
        Assert.assertEquals(30, basket.getBasket().getTotalPrice());
        basket.addProduct(BREAD.getName(), 1);
        basket.addProduct(BREAD.getName(), 1);
        Assert.assertEquals(3, basket.getBasket().getBasket().size());
        Assert.assertEquals(3, this.getFromBasket(BREAD.getName()).getQuantity());
        Assert.assertEquals(35, basket.getBasket().getTotalPrice());
        basket.removeProduct(COLA.getName(), 1);
        Assert.assertEquals(2, basket.getBasket().getBasket().size());
        Assert.assertEquals(30, basket.getBasket().getTotalPrice());
        verify(TestContextBeanConfiguration.getProductServiceConnector(), times(5)).getProduct(any());
    }

    @Test
    public void testCalculateSpecialOfferForCola() throws RestControllerException {
        when(TestContextBeanConfiguration.getProductServiceConnector().getProduct(COLA.getName())).thenReturn(COLA.getProduct());
        basket.addProduct(COLA.getName(), 1);
        Assert.assertEquals(5, basket.getBasket().getTotalPrice());
        Assert.assertEquals(5, basket.getBasket().getBasket().get(0).getPriceForProduct());
        basket.addProduct(COLA.getName(), 3);
        Assert.assertEquals(20, basket.getBasket().getTotalPrice());
        Assert.assertEquals(20, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(4, basket.getBasket().getBasket().get(0).getQuantity());
        basket.addProduct(COLA.getName(), 1);
        Assert.assertEquals(20, basket.getBasket().getTotalPrice());
        Assert.assertEquals(20, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(5, basket.getBasket().getBasket().get(0).getQuantity());
        basket.removeProduct(COLA.getName(), 1);
        Assert.assertEquals(20, basket.getBasket().getTotalPrice());
        Assert.assertEquals(20, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(4, basket.getBasket().getBasket().get(0).getQuantity());
        basket.removeProduct(COLA.getName(), 1);
        Assert.assertEquals(15, basket.getBasket().getTotalPrice());
        Assert.assertEquals(15, basket.getBasket().getBasket().get(0).getPriceForProduct());
        Assert.assertEquals(3, basket.getBasket().getBasket().get(0).getQuantity());
        basket.removeProduct(COLA.getName(), 3);
        Assert.assertEquals(0, basket.getBasket().getTotalPrice());
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        verify(TestContextBeanConfiguration.getProductServiceConnector(),
                times(3)).getProduct(COLA.getName());
    }

    @Test
    public void testRemovingMoreThanAdded() throws RestControllerException {
        when(TestContextBeanConfiguration
                .getProductServiceConnector().getProduct(MOBILE.getName())).thenReturn(MOBILE.getProduct());
        basket.addProduct(MOBILE.getName(), 1);
        Assert.assertEquals(5, basket.getBasket().getBasket().get(0).getPriceForProduct());
        basket.removeProduct(MOBILE.getName(), 2);
        Assert.assertEquals(0, basket.getBasket().getTotalPrice());
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        verify(TestContextBeanConfiguration.getProductServiceConnector(),
                times(1)).getProduct(MOBILE.getName());
    }

    @Test
    public void testCalculateSpecialOfferManyGoodsAtOnce() throws RestControllerException {
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BREAD.getName())).thenReturn(BREAD.getProduct());
        basket.addProduct(BREAD.getName(), 4);
        when(TestContextBeanConfiguration.getProductServiceConnector()
                .getProduct(BUTTER.getName())).thenReturn(BUTTER.getProduct());
        basket.addProduct(BUTTER.getName(), 7);
        Assert.assertEquals(95, this.getFromBasket(BUTTER.getName()).getPriceForProduct());
        Assert.assertEquals(25, this.getFromBasket(BREAD.getName()).getPriceForProduct());
        basket.removeProduct(BREAD.getName(), 2);
        Assert.assertEquals(20, this.getFromBasket(BREAD.getName()).getPriceForProduct());
        verify(TestContextBeanConfiguration.getProductServiceConnector(),
                times(2)).getProduct(any());
    }

    @Test
    public void testRemoveOnEmpty() throws RestControllerException {
        basket.removeProduct(BREAD.getName(), 1);
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        Assert.assertEquals(0, basket.getBasket().getTotalPrice());
        verify(TestContextBeanConfiguration.getProductServiceConnector(),
                times(0)).getProduct(BREAD.getName());
    }

    @Test
    public void testRemoveZeroItems() throws RestControllerException {
        basket.removeProduct(BREAD.getName(), 0);
        Assert.assertEquals(0, basket.getBasket().getBasket().size());
        Assert.assertEquals(0, basket.getBasket().getTotalPrice());
        verify(TestContextBeanConfiguration.getProductServiceConnector(),
                times(0)).getProduct(BREAD.getName());
    }

    private BasketGood getFromBasket(String name) {
        return this.basket.getBasket().getBasket()
                .stream()
                .filter(good -> good.getName()
                        .equals(name))
                .limit(1)
                .collect(Collectors.toList()).get(0);
    }

}
