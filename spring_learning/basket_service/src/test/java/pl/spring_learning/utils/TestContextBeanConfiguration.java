package pl.spring_learning.utils;

import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.spring_learning.service.BasketService;
import pl.spring_learning.service.IBasketService;
import pl.spring_learning.service.IProductServiceConnector;

/**
 * @Author Lukasz Wojtas
 * */

@Configuration
@EnableAutoConfiguration
public class TestContextBeanConfiguration {

    private static final IProductServiceConnector PRODUCT_SERVICE_CONNECTOR = Mockito.mock(IProductServiceConnector.class);

    @Bean
    IBasketService getBasketService() {
        return new BasketService(PRODUCT_SERVICE_CONNECTOR);
    }

    public static IProductServiceConnector getProductServiceConnector() {
        return PRODUCT_SERVICE_CONNECTOR;
    }
}

