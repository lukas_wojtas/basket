package pl.spring_learning.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.spring_learning.properties.ProductServiceProperties;
import pl.spring_learning.service.ProductServiceConnector;

@Configuration
public class OkHttpClientConfiguration {

    private OkHttpClient httpClient = Mockito.mock(OkHttpClient.class);

    @Bean
    public ProductServiceConnector getConnector() {
        ProductServiceProperties props = new ProductServiceProperties();
        props.setHost("host");
        props.setPort(666);
        ObjectMapper mapper = new ObjectMapper();
        return new ProductServiceConnector(props, httpClient, mapper);
    }

    @Bean
    public OkHttpClient okHttpClient() { return httpClient; }
}
