package pl.spring_learning.persistence.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * @Author Lukasz Wojtas
 * */
@Data
@Configuration
@ConfigurationProperties
public class PersistenceProperties {
    @Value("${datasource.url}")
    private String url;
    @Value("${datasource.driverClassName}")
    private String driverClassName;
    @Value("${datasource.user}")
    private String user;
    @Value("${datasource.password}")
    private String password;
    @Value("${datasource.hikari.minimumIdle}")
    private Integer hikariProps;
    @Value("${datasource.hikari.maximumPoolSize}")
    private Integer maximumPoolSize;
    @Value("${datasource.hikari.initializationFailTimeout}")
    private Integer initializationFailTimeout;
    @Value("${datasource.hikari.poolName}")
    private String poolName;
    @Value("${datasource.hikari.connectionTestQuery}")
    private String connectionTestQuery;
    @Value("${datasource.hikari.connectionTimeout}")
    private Integer connectionTimeout;
    @Value("${datasource.hikari.minimumIdle}")
    private Integer minimumIdle;
    @Value("${datasource.enableStatistics}")
    private boolean enableStatistics;
}
