/*
 * @author Lukasz Wojtas
 * To private use only.
 */

package pl.spring_learning.persistence.dao.product;

import lombok.extern.slf4j.Slf4j;
import pl.spring_learning.core.dao_abstraction.IProductDaoReader;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.persistence.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
public final class ProductDaoReader implements IProductDaoReader {

    private final ProductRepository repository;

    public ProductDaoReader(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<ProductEntity> getProduct(String name) {
        return repository.findById(name);
    }

    @Override
    public List<ProductEntity> getAllProducts() {
        return repository.findAll();
    }

    @Override
    public Boolean checkIfProductInStock(String name) {
        return repository.existsById(name);
    }
}
