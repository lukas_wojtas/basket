package pl.spring_learning.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import pl.spring_learning.entity.PersonEntity;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */
public interface PersonRepository extends CrudRepository<PersonEntity, Integer> {

    @NonNull
    @Override
    List<PersonEntity> findAll();
}
