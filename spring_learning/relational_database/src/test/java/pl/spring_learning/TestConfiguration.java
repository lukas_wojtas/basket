package pl.spring_learning;

import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.spring_learning.persistence.properties.PersistenceProperties;
/**
 * @Author Lukasz Wojtas
 * */
@Configuration
public class TestConfiguration {

    private final static SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);

    private final static Statistics statistics = Mockito.mock(Statistics.class);

    @Bean
    SessionFactory getSessionFactoryBean() {
        Mockito.when(sessionFactory.getStatistics()).thenReturn(statistics);
        return sessionFactory;
    }

    @Bean
    PersistenceProperties getHibernateProperties() {
        PersistenceProperties props = new PersistenceProperties();
        props.setEnableStatistics(false);
        return props;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Statistics getStatistics() {
        return statistics;
    }

}
