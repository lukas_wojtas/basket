package pl.spring_learning;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.spring_learning.core.dao_abstraction.IPersonDaoReader;
import pl.spring_learning.core.dao_abstraction.IPersonDaoWriter;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import pl.spring_learning.entity.PersonEntity;
import pl.spring_learning.persistence.PersistenceConfig;
import pl.spring_learning.persistence.repository.PersonRepository;
import pl.spring_learning.persistence.properties.PersistenceProperties;

import java.util.List;

/**
 * @Author Lukasz Wojtas
 * */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        PersistenceConfig.class,
        PersistenceProperties.class,
        PersonRepository.class
})
@Ignore
public class TestPersonDaoLiveDataBase extends AbstractTestNGSpringContextTests {

    @Autowired
    private IPersonDaoWriter personDaoWriter;

    @Autowired
    private IPersonDaoReader personDaoReader;

    @Autowired
    private PersistenceProperties props;

    @BeforeMethod(groups = "LiveDataSource")
    public void setUp(){
        List<PersonEntity> list = personDaoReader.getPeople();
        personDaoWriter.removePeople(list);
    }

    @Test(groups = {"LiveDataSource"})
    public void testCreateDeletePerson(){
        PersonEntity person = new PersonEntity();
        person.setName("name");
        personDaoWriter.savePerson(person);
        personDaoWriter.getPerson(person.getId()).ifPresentOrElse(p -> {
            Assert.assertEquals(person.getId(), p.getId());
            Assert.assertEquals(person.getName(), p.getName());
            List<PersonEntity> list = personDaoReader.getPeople();
            Assert.assertEquals(1, list.size());
            personDaoWriter.removePerson(person);
            list = personDaoReader.getPeople();
            Assert.assertEquals(0, list.size());
        }, Assert::fail);
    }
}
