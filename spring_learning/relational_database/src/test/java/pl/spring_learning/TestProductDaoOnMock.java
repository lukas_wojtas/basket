package pl.spring_learning;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import pl.spring_learning.entity.ProductEntity;
import pl.spring_learning.persistence.dao.product.ProductDaoReader;
import pl.spring_learning.persistence.dao.product.ProductDaoWriter;
import pl.spring_learning.persistence.repository.ProductRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class TestProductDaoOnMock {

    private static final String ID = "name";

    private ProductDaoReader productDaoReader;

    private ProductDaoWriter productDaoWriter;

    //private final AbstractDataSource dataSource = Mockito.mock(AbstractDataSource.class);

    private final ProductRepository repository = Mockito.mock(ProductRepository.class);

    private ProductEntity productEntity;

    private static final String NO_CONNECTION_TO_DATABASE = "No connection to database";

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        Mockito.reset(repository);
        productDaoReader = new ProductDaoReader(repository);
        productDaoWriter = new ProductDaoWriter(repository);
        productEntity = new ProductEntity();
    }

    @Test
    public void testGetProduct() {
        productEntity.setName(ID);
        when(repository.findById(ID)).thenReturn(Optional.of(productEntity));
        Optional<ProductEntity> optionalProduct = productDaoReader.getProduct(ID);
        Assert.assertTrue(optionalProduct.isPresent());
        Assert.assertEquals(ID, optionalProduct.get().getName());
    }

    @Test
    public void testGetProductOnEmpty() {
        when(repository.findById(ID)).thenReturn(Optional.empty());
        Optional<ProductEntity> optionalProduct = productDaoReader.getProduct(ID);
        Assert.assertFalse(optionalProduct.isPresent());
    }

    @Test
    public void testGetProductWithException() {
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        this.thrown.expect(RuntimeException.class);
        when(repository.findById(ID))
                .thenThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE));
        Optional<ProductEntity> optionalProduct = productDaoReader.getProduct(ID);
        Assert.assertFalse(optionalProduct.isPresent());
    }

    @Test
    public void getAllProducts() {
        when(repository.findAll()).thenReturn(Collections.singletonList(new ProductEntity()));
        Assert.assertEquals(1, productDaoReader.getAllProducts().size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void getAllProductsWithException() {
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        this.thrown.expect(RuntimeException.class);
        when(repository.findAll())
                .thenThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE));
        productDaoReader.getAllProducts();
    }

    @Test
    public void testAddProduct() {
        productDaoWriter.addProduct(productEntity);
        verify(repository, times(1)).save(productEntity);
    }

    @Test
    public void testAddProductWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository)
                .save(productEntity);
        productDaoWriter.addProduct(productEntity);
    }

    @Test
    public void testAddProducts() {
        List<ProductEntity> entityList = Collections.singletonList(productEntity);
        productDaoWriter.addProducts(entityList);
        verify(repository, times(1)).saveAll(entityList);
    }

    @Test
    public void testAddProductsWitException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        List<ProductEntity> list = Collections.singletonList(productEntity);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository)
                .saveAll(list);
        productDaoWriter.addProducts(list);
    }

    @Test
    public void testRemoveProduct() {
        productDaoWriter.removeProduct(productEntity);
        verify(repository, times(1)).delete(productEntity);
    }

    @Test
    public void testRemoveProductWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository)
                .delete(productEntity);
        productDaoWriter.removeProduct(productEntity);
    }

    @Test
    public void testRemoveProducts() {
        List<ProductEntity> list = Collections.singletonList(productEntity);
        productDaoWriter.removeProducts(list);
        verify(repository, times(1)).deleteAll(list);
    }

    @Test
    public void testRemoveProductsWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        List<ProductEntity> list = Collections.singletonList(productEntity);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository)
                .deleteAll(list);
        productDaoWriter.removeProducts(list);
    }

    @Test
    public void testUpdateProduct() {
        productDaoWriter.updateProduct(productEntity);
        verify(repository, times(1)).save(productEntity);
    }

    @Test
    public void testUpdateProductWithException() {
        this.thrown.expect(RuntimeException.class);
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        doThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE))
                .when(repository)
                .save(productEntity);
        productDaoWriter.updateProduct(productEntity);
    }

    @Test
    public void testCheckProductInStock() {
        productEntity.setName(ID);
        when(repository.existsById(ID)).thenReturn(true);
        Assert.assertTrue(productDaoReader.checkIfProductInStock(ID));
    }

    @Test
    public void testCheckIfNoProductInStock() {
        when(repository.findById(ID)).thenReturn(Optional.empty());
        Assert.assertFalse(productDaoReader.checkIfProductInStock(ID));
    }

    @Test
    public void testCheckIfProductInStockWithException() {
        this.thrown.expectMessage(NO_CONNECTION_TO_DATABASE);
        this.thrown.expect(RuntimeException.class);
        when(repository.existsById(ID))
                .thenThrow(new RuntimeException(NO_CONNECTION_TO_DATABASE));
        Assert.assertFalse(productDaoReader.checkIfProductInStock(ID));
    }
}
