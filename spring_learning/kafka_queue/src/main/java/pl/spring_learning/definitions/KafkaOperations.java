package pl.spring_learning.definitions;

/**
 * @Author Lukasz Wojtas
 * */

public enum KafkaOperations {
    CREATE_PRODUCT, DELETE_PRODUCT
}
