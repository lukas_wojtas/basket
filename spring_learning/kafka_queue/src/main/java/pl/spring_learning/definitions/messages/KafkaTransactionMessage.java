package pl.spring_learning.definitions.messages;

import lombok.Data;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTransactionResult;
import javax.validation.constraints.NotNull;

/**
 * @Author Lukasz Wojtas
 * */
@Data
public class KafkaTransactionMessage {
    @NotNull
    private String transactionId;
    @NotNull
    private KafkaTransactionResult result;
    @NotNull
    private String payload;
    @NotNull
    private KafkaOperations operation;
}
