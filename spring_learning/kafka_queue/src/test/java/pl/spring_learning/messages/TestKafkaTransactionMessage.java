package pl.spring_learning.messages;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.spring_learning.definitions.KafkaOperations;
import pl.spring_learning.definitions.KafkaTransactionResult;
import pl.spring_learning.definitions.messages.KafkaTransactionMessage;
import pl.spring_learning.validator.DataValidator;
import pl.spring_learning.validator.ValidationException;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;

/**
 * @Author Lukasz Wojtas
 * */

public class TestKafkaTransactionMessage {

    private DataValidator validator;

    @Before
    public void setUp() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = new DataValidator(validatorFactory);
    }

    @Test
    public void testFullKafkaMessage() throws ValidationException {
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setPayload("payload");
        message.setOperation(KafkaOperations.CREATE_PRODUCT);
        message.setResult(KafkaTransactionResult.SUCCESS);
        message.setTransactionId("transactionId");
        Assert.assertTrue(validator.validate(message));
    }

    @Test(expected = ValidationException.class)
    public void testKafkaMessageWithoutOperation() throws ValidationException {
        KafkaTransactionMessage message = new KafkaTransactionMessage();
        message.setPayload("payload");
        message.setTransactionId("transactionId");
        validator.validate(message);
    }
}
